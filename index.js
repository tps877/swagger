var path=require('path')
var express=require('express')
var app=express()

var swaggerJsdoc=require('swagger-jsdoc')
var swaggerUi=require('swagger-ui-express')

var userRouter=require('./routes/users')
app.use('/users',userRouter)


var options={
	definition:{
		openapi:'3.0.0',
		info:{
			title:'测试使用express项目启动swagger',
			version:'1.0.0',
			description:`  
                 测试项目
                 使用swagger生成api文档
			`
		}
	},
	// 去哪个路由下收集 swagger 注释
	 apis:[path.join(__dirname, 'routes/*.js')]
}

var swaggerSpec=swaggerJsdoc(options)
app.use('/api-docs',swaggerUi.serve,swaggerUi.setup(swaggerSpec))

app.listen(3000,()=>{
	console.log('服务启动')
})