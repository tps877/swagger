var express=require('express')
var router=express.Router()

 /**,
 * @swagger
 * /users/hello:
 *    get:
 *      tags:
 *      - 打招呼
 *      summary: 打招呼方法
 *      produces:
 *      - application/json
 *      parameters:
 *      - name: name
 *        in: query
 *        description: 姓名
 *        required: true
 *        type: string
 *        maximum:
 *        minimum: 1
 *        format:
 *      responses:
 *        200:
 *          description: successful operation
 *          schema:
 *            ref: #/definitions/Order
 *        400:
 *          description: Invalid ID supplied
 *        404:
 *          description: Order not found
 * */

router.get('/hello',(req,res)=>{
	let name=req.query.name
	res.send(`hello ${name}`)
})


module.exports=router